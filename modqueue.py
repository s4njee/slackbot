# This code runs continually, running modqueue_to_db and modqueue_reactions.
# This is to allow for simple debugging and also to keep code segmented.

import praw, prawcore, sys
import time as pytime

while True:
    exec (open('modqueue_to_db.py').read())
    pytime.sleep(1)
    exec (open('modqueue_reactions.py').read())
