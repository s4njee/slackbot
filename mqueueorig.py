# This code reads in the modqueue (reported/filtered comments, reported posts, and unmoderated posts).
# It reports these to #mod-queue and appends text to the message sent there.

import praw, prawcore, pymysql, sys, time, re
from automod import modqueue_message
import config

modqueue_channel = config.modqueue_channel
notification_channel = config.notification_channel

mod_db = pymysql.connect(host='localhost', user=config.mod_db_user, passwd=config.mod_db_pass, db=config.mod_db_name,
                         autocommit=True, charset='utf8mb4')
mod_cursor = mod_db.cursor()

if config.automod_enabled == True:
    automod_db = pymysql.connect(host='localhost', user=config.automod_db_user, passwd=config.automod_db_pass,
                                 db=config.automod_db_name, autocommit=True, charset='utf8')
    automod_cursor = automod_db.cursor()

    filtered_phrases = []
    automod_cursor.execute("""SELECT * FROM `filtered_phrases`""")

    for phrase in automod_cursor:
        filtered_phrases.append(phrase[1])

# Check if login worked.
# PRAW doesn't detect if a login was wrong.
# You have to *do* something with the login for PRAW to throw an error.
# Thus the reddit.user.me() line.
try:
    reddit = praw.Reddit(client_id=config.reddit_client_id,
                         client_secret=config.reddit_client_secret,
                         username=config.reddit_user,
                         password=config.reddit_pass,
                         user_agent='Moderation Queue to Database v 1.0 by /u/' + config.reddit_user)

    reddit.user.me()

# Otherwise, send a message to #notifications to alert to the error and close out the dbs and cursors.
except:
    mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""",
                       ('chat.postMessage', notification_channel, 'PRAW login error on modqueue_to_db'))

    if config.automod_enabled == True:
        automod_cursor.close()
        automod_db.close()

    mod_cursor.close()
    mod_db.close()

    sys.exit()

# Get reported and filtered comments
for reported_comment in reddit.subreddit('CFB').mod.modqueue('comments', limit=None):
    # Extract the comment and link ids and report time
    comment_id = reported_comment.name.replace('t1_', '')
    post_id = reported_comment.link_id.replace('t3_', '')
    report_time = str(int(time.time()))

    report_reasons = []

    if config.automod_enabled == True:
        modqueue_message_text = modqueue_message(reported_comment.body)
    else:
        modqueue_message_text = ""

    # This gets the user reports and appends the appropriate text
    # reports[0] is the report reason
    for reports in reported_comment.user_reports:
        report_reasons.append(reports[0])

    # This checks if the comment was removed for reddit-wide reasons
    if reported_comment.banned_by == True:
        report_reasons.append("reddit-wide removal rule")

    # This checks if the comment was removed by AutoModerator and appends the appropriate text
    elif reported_comment.banned_by != True and modqueue_message_text != '' and reported_comment.mod_reports == [] and config.automod_enabled == True:
        report_reasons.append(modqueue_message_text + " / AutoModerator")

    # This checks if the comment was reported by a non-AutoModerator moderator and appends the appropriate text
    # reports[0] is the report reason, reports[1] is the moderator in question
    else:
        for reports in reported_comment.mod_reports:
            report_reasons.append(reports[0] + " / " + reports[1])

    try:
        report_reasons = ", ".join(report_reasons)
    except:
        report_reasons = ""
    report_reasons = report_reasons.replace("'", "").replace('"', '')

    # reddit URL for comment and chat message text
    url = "https://old.reddit.com/comments/" + post_id + "//" + comment_id + "?context=5"
    text = "<" + url + "|*Comment by " + reported_comment.author.name + ":* " + reported_comment.body[:200].replace("'",
                                                                                                                    "").replace(
        '"', '').replace('\n', ' ').replace('>', '').replace('<', '&lt;').replace('&', '&amp;') + ">"
    text += " (" + report_reasons + ")"

    # Check if comment is already in the db as reported
    db_check = mod_cursor.execute(
        """SELECT `report_time`, `message_time`, `report_text` FROM `modqueue` WHERE `post_id` = %s AND `comment_id` = %s""",
        (post_id, comment_id))

    # If the comment is not already in the db, insert the message into actions table and insert the reported comment into modqueue table
    if db_check == 0:
        mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""",
                           ('chat.postMessage', modqueue_channel, text))
        actions_id = str(mod_cursor.lastrowid)
        mod_cursor.execute(
            """INSERT INTO `modqueue` (`post_id`, `comment_id`, `report_time`, `slack_action_id`, `reported`, `report_text`) VALUES (%s, %s, %s, %s, %s, %s)""",
            (post_id, comment_id, report_time, actions_id, 'Yes', text))

    # If the comment is already in the db but has not been moderated, this will delete the old message and modqueue item and redo both to bring attention.
    elif db_check != 0:
        report_time = mod_cursor.fetchall()[0]
        message_time = report_time[1]
        old_text = report_time[2]
        report_time = report_time[0]

        # If the comment has gone unmoderated for some time (per config) or a new report has come in, put it back in #mod-queue for attention.
        if time.time() - float(report_time) >= config.mins_until_bump * 60.0 or old_text != text:
            report_time = str(int(time.time()))
            mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_time`) VALUES (%s, %s, %s)""",
                               ('chat.delete', modqueue_channel, message_time))
            mod_cursor.execute("""DELETE FROM `modqueue` WHERE `post_id` = %s AND `comment_id` = %s""",
                               (post_id, comment_id))
            mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""",
                               ('chat.postMessage', modqueue_channel, text))
            actions_id = str(mod_cursor.lastrowid)
            mod_cursor.execute(
                """INSERT INTO `modqueue` (`post_id`, `comment_id`, `report_time`, `slack_action_id`, `reported`, `report_text`) VALUES (%s, %s, %s, %s, %s, %s)""",
                (post_id, comment_id, report_time, actions_id, 'Yes', text))

# Get reported posts
for reported_post in reddit.subreddit('CFB').mod.modqueue('submissions', limit=None):
    # Extract the link id and report time
    post_id = reported_post.name.replace('t3_', '')
    report_time = str(int(time.time()))

    report_reasons = []

    for reports in reported_post.user_reports:
        report_reasons.append(reports[0])

    for reports in reported_post.mod_reports:
        report_reasons.append(reports[0] + " / " + reports[1])

    report_reasons = ", ".join(report_reasons)

    # reddit URL for post and chat message text
    url = "https://old.reddit.com/comments/" + post_id + "//"
    text = "<" + url + "|*Submission by " + reported_post.author.name + ":* " + reported_post.title[:200].replace("'",
                                                                                                                  "").replace(
        '"', '').replace('\n', ' ').replace('>', '').replace('<', '&lt;').replace('&', '&amp;') + ">"
    text += " (" + report_reasons.replace("'", "").replace('"', '') + ")"

    # Check if post is already in the db as unmoderated/reported
    db_check = mod_cursor.execute(
        """SELECT `report_time`, `message_time`, `reported` FROM `modqueue` WHERE `post_id` = %s AND `comment_id` = %s""",
        (post_id, ''))

    # If the post is not already in the db, insert the message into actions table and insert the reported post into modqueue table
    if db_check == 0:
        mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""",
                           ('chat.postMessage', modqueue_channel, text))
        actions_id = str(mod_cursor.lastrowid)
        mod_cursor.execute(
            """INSERT INTO `modqueue` (`post_id`, `comment_id`, `report_time`, `slack_action_id`, `reported`) VALUES (%s, %s, %s, %s, %s)""",
            (post_id, '', report_time, actions_id, 'Yes'))

    # If the post is already in the db but has not been moderated, this will delete the old message and modqueue item and redo both to bring attention.
    elif db_check != 0:
        report_time = mod_cursor.fetchall()[0]
        report = report_time[2]
        message_time = report_time[1]
        report_time = report_time[0]

        # If the post has been reported while unmoderated or has gone unmoderated for some time (per config), put it back in #mod-queue for attention.
        if report == 'No' or time.time() - float(report_time) >= config.mins_until_bump * 60.0:
            report_time = str(int(time.time()))
            mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_time`) VALUES (%s, %s, %s)""",
                               ('chat.delete', modqueue_channel, message_time))
            mod_cursor.execute("""DELETE FROM `modqueue` WHERE `post_id` = %s AND `comment_id` = %s""", (post_id, ''))
            mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""",
                               ('chat.postMessage', modqueue_channel, text))
            actions_id = str(mod_cursor.lastrowid)
            mod_cursor.execute(
                """INSERT INTO `modqueue` (`post_id`, `comment_id`, `report_time`, `slack_action_id`, `reported`) VALUES (%s, %s, %s, %s, %s)""",
                (post_id, '', report_time, actions_id, 'Yes'))

# Get unmoderated posts
for unmoderated_post in reddit.subreddit('CFB').mod.unmoderated():
    # Extract the link id and post time
    post_id = unmoderated_post.name.replace('t3_', '')
    report_time = str(int(time.time()))

    # reddit URL for comment and chat message text
    url = "https://old.reddit.com/comments/" + post_id + "//"
    text = "<" + url + "|*Submission by " + unmoderated_post.author.name + ":* " + unmoderated_post.title[:100].replace(
        "'", "").replace('"', '').replace('\n', ' ').replace('>', '').replace('<', '&lt;').replace('&', '&amp;') + ">"

    # Check if the post is already in the db as unmoderated
    db_check = mod_cursor.execute(
        """SELECT `report_time`, `message_time` FROM `modqueue` WHERE `post_id` = %s AND `comment_id` = %s""",
        (post_id, ''))

    # If the post is not already in the db, insert the message into actions table and insert the unmoderated post into modqueue table
    if db_check == 0:
        mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""",
                           ('chat.postMessage', modqueue_channel, text))
        actions_id = str(mod_cursor.lastrowid)
        mod_cursor.execute(
            """INSERT INTO `modqueue` (`post_id`, `comment_id`, `report_time`, `slack_action_id`, `reported`) VALUES (%s, %s, %s, %s, %s)""",
            (post_id, '', report_time, actions_id, 'No'))

    # If the comment is already in the db but has not been moderated, this will delete the old message and modqueue item and redo both to bring attention.
    elif db_check != 0:
        report_time = mod_cursor.fetchall()[0]
        message_time = report_time[1]
        report_time = report_time[0]

        # If the comment has gone unmoderated for some time (per config), put it back in #mod-queue for attention.
        if time.time() - float(report_time) >= config.mins_until_bump * 60.0:
            report_time = str(int(time.time()))
            mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_time`) VALUES (%s, %s, %s)""",
                               ('chat.delete', modqueue_channel, message_time))
            mod_cursor.execute("""DELETE FROM `modqueue` WHERE `post_id` = %s AND `comment_id` = %s""", (post_id, ''))
            mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""",
                               ('chat.postMessage', modqueue_channel, text))
            actions_id = str(mod_cursor.lastrowid)
            mod_cursor.execute(
                """INSERT INTO `modqueue` (`post_id`, `comment_id`, `report_time`, `slack_action_id`, `reported`) VALUES (%s, %s, %s, %s, %s)""",
                (post_id, '', report_time, actions_id, 'No'))

if config.automod_enabled == True:
    automod_cursor.close()
    automod_db.close()

mod_cursor.close()
mod_db.close()
