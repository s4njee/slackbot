import praw, prawcore, json
import config
import pymongo

client = pymongo.MongoClient(config.mongodb)
db = client.get_database()
reddit = praw.Reddit(client_id=config.reddit_client_id,
                     client_secret=config.reddit_client_secret,
                     username=config.reddit_user,
                     password=config.reddit_pass,
                     user_agent='Get flairs by /u/' + config.reddit_user)


def get_user_flairs():
    for flair in reddit.subreddit(config.subreddit).flair(limit=None):
        print(str(flair['user']))

        db['user_flair'].update_one(
            {'username': str(flair['user'])},
            {'$set': {'flair_text': flair['flair_text'], 'flair_css_class': flair['flair_css_class']}},
            upsert=True)

