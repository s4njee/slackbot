# This code reads in the actions table and performs the action, be it leaving a message or adding a reaction.
# This code runs continually.
# The reason for this code is to allow our various Slack-related files to send Slack messages without calling the Slack API over and over.
# Calling the Slack API repeatedly is slow, this should create a significant speed increase.
# This also prevents us from having to read in files all over the place.

import sys
import config
import commands
import pymongo
# Added scheduling code
from apscheduler.schedulers.blocking import BlockingScheduler
import logging
from get_user_flair import get_user_flairs

log = logging.getLogger('apscheduler.executors.default')
#log.setLevel(logging.INFO)  # DEBUG
fmt = logging.Formatter('%(levelname)s:%(name)s:%(message)s')
h = logging.StreamHandler()
h.setFormatter(fmt)
log.addHandler(h)
sched = BlockingScheduler()

client = pymongo.MongoClient(config.mongodb)
db = client.get_database()
notification_channel = config.notification_channel


# if config.custom_commands == True:
#    mod_cursor.execute("""SELECT `command_text` FROM `commands`""")
#    commands_list = mod_cursor.fetchall()
#    chat_commands = []
#
#    for command in commands_list:
#        chat_commands.append(command[0])


@sched.scheduled_job('interval', seconds=5)
def slackpy():
    history_count = db['history'].count()

    # Delete messages past 150 to keep from using all of the message space in case something goes wrong.
    if history_count > 150:
        history = db['history'].find({}).sort('message_time', pymongo.ASCENDING).limit(history_count - 150)

        for message in history:
            slack_client.api_call('chat.delete', channel=message['channel'],
                                  ts=format(float(message['message_time']), '.6f'))
            db['history'].delete_one(
                {'channel': message['channel'], 'message_time': str(float(message['message_time']))})

    # Get the Slack actions (post message, add reaction, delete message, etc.)
    actions = db['actions'].find({})
    for action in actions:
        # action[1] = action (chat.postMessage, chat.delete, reactions.add, etc.)
        # action[2] = channel (#bottesting, #mod-queue, etc.)
        # action[3] = message_text (DEFAULT NULL, #mod-queue report, text for !dearcfb, etc.)
        # action[4] = reaction_name (DEFAULT NULL, :approved:, :removed:, :rip:, etc.)
        # action[5] = reaction_time (DEFAULT NULL, time in epoch time (seconds) for the reactions)
        # action[6] = message_time (DEFUALT NULL, time in epoch time (seconds) for the message)

        # If adding a reaction (approved, removed, etc.) to an already posted message, do this.
        if action['reaction_time'] != "":
            print(action['reaction_time'])
            time = format(float(action['reaction_time']), '.6f')
            message = slack_client.api_call(action['action'], channel=action['channel'], name=action['reaction_name'],
                                            timestamp=time,
                                            as_user="true")

        # If making a new message in the channel, do this.
        elif action['action'] == 'chat.delete':
            slack_client.api_call(action['action'], channel=action['channel'], ts=float(action['message_time']),
                                  as_user="true")
            try:
                db['history'].delete_one(({'$and': [{'channel': action['channel']},
                                                    {'message_time': str(float(action['message_time']))}]}))
            except Exception as e:
                print("text1" + str(e))
                slack_client.api_call('chat.postMessage', channel=notification_channel,
                                      text='Could not delete from history', as_user="true", unfurl_links="false")

        else:
            message = slack_client.api_call(action['action'], channel=action['channel'],
                                            text=action['message_text'], name=action['reaction_name'],
                                            as_user="true", unfurl_links="false")

            # This is for new modqueue messages. The modqueue table has a column for message time.
            # If the message is for a modqueue item, update the table with the Slack message time so that reactions can be added to it once acted on.
            try:
                message_time = str(message['ts'])
                try:
                    try:
                        test = db['modqueue'].update_one({"slack_action_id": action['_id']}, {'$set': {'message_time': message_time}})
                        print(test.modified_count)
                    except Exception as e:
                        print("text2" + str(e))
                        slack_client.api_call('chat.postMessage', channel=notification_channel,
                                              text='Could not update modqueue with slack_action_id', as_user="true",
                                              unfurl_links="false")
                    db['history'].insert_one({'channel': action['channel'], 'message_time': message_time})
                except Exception as e:
                    print("text3" + str(e))
                    slack_client.api_call('chat.postMessage', channel=notification_channel,
                                          text='Could not insert this into history', as_user="true",
                                          unfurl_links="false")
            except Exception as e:
                print("text4" + str(e))
                pass

        # After the message has been sent, delete it from the actions database queue.
        test = db['actions'].delete_one({'_id': action['_id']})

@sched.scheduled_job('interval', seconds=5)
def slackbotpy():
    # Read in all messages that bot can see
    message = slack_client.rtm_read()

    try:
        if message:
            # Get the channel and text for the message by a mod
            channel = message[0]['channel']
            message_text = message[0]['text']

            # If the message starts with !u, output the user's information
            if message_text.startswith('!u'):
                try:
                    username = message[0]['text'].split(' ')[1]
                    if config.flair_enabled == True:
                        flair_text = commands.flair(username)
                    else:
                        flair_text = ''
                    if config.rules_enabled == True:
                        rules_text = commands.rules(username)
                    else:
                        rules_text = ''
                    if config.notes_enabled == True:
                        notes_text = commands.notes(username)
                    else:
                        notes_text = ''
                    bans_text = commands.bans(username)
                    warnings_text = commands.warnings(username)
                    removals_text = commands.removals(username)

                    text = "*Overview for <https://old.reddit.com/u/" + username + "/overview/|" + username + ">*\n"
                    if config.flair_enabled == True:
                        text += flair_text + "\n"
                    if config.rules_enabled == True:
                        text += rules_text + "\n"
                    if config.notes_enabled == True:
                        text += notes_text + "\n"
                    text += bans_text + "\n"
                    text += warnings_text + "\n"
                    text += removals_text

                    db['actions'].insert_one({'action': 'chat.postMessage', 'channel': channel,
                                                  'message_text': text, 'reaction_name': None, "reaction_time": None})

                except Exception as e:
                    print("firstException"+str(e))
                    slack_client.api_call('chat.postMessage', channel=notification_channel,
                                          text='Could not perform action !u', as_user="true")


            # If the message starts with !ban, execute the in-Slack reddit ban from CFB_Referee.
            #			elif message_text.startswith('!ban '):
            #				message_text = message_text[9:]
            #				username = message_text.split(' | ')[0]
            #				length = message_text.split(' | ')[1]
            #
            #				try:
            #					length = int(length)
            #				except Exception as e:
            #					pass
            #
            #				reason = message_text.split(' | ')[2]
            #				ban_message = message_text.split(' | ')[3]
            #				text = commands.ban_user('SometimesWhyy', length, reason, ban_message)
            #				print(text)
            #
            #				mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""", ('chat.postMessage', channel, text))

            # If instead the message is a different Slack command (e.g. !dearcfb), output the relevant text to Slack
            # elif config.custom_commands == True and message_text in chat_commands:
            #     try:
                #     mod_cursor.execute("""SELECT * FROM `commands` WHERE `command_text` = %s""", message_text)
                #     text = mod_cursor.fetchall()[0][2]
                #     mod_cursor.execute(
                #         """INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""",
                #         ('chat.postMessage', channel, text))
                #
                # except Exception as e:
                #     slack_client.api_call('chat.postMessage', channel=notification_channel,
                #                           text='Could not output command', as_user="true")

    except Exception as e:
        print("lastException"+str(e))
        pass

@sched.scheduled_job('interval', seconds=5)
def modqueuepy():
    exec(open('modqueue_to_db.py').read())
    exec(open('modqueue_reactions.py').read())


@sched.scheduled_job('interval', days=1)
def get_flairs():
    get_user_flairs()


if 'slackclient' not in sys.modules:
    import slackclient
    from slackclient import SlackClient

    slack_client = SlackClient(config.client_token)
    # Start the scheduler
    if slack_client.rtm_connect():
        sched.start()
