# This code reads in the modqueue (reported/filtered comments, reported posts, and unmoderated posts).
# It reports these to #mod-queue and appends text to the message sent there.

import praw, prawcore, sys, time, re
import config
import pymongo

client = pymongo.MongoClient(config.mongodb)
db = client.get_database()
modqueue_channel = config.modqueue_channel
notification_channel = config.notification_channel

# Check if login worked.
# PRAW doesn't detect if a login was wrong.
# You have to *do* something with the login for PRAW to throw an error.
# Thus the reddit.user.me() line.
try:
    reddit = praw.Reddit(client_id=config.reddit_client_id,
                         client_secret=config.reddit_client_secret,
                         username=config.reddit_user,
                         password=config.reddit_pass,
                         user_agent='Moderation Queue to Database v 1.0 by /u/' + config.reddit_user)

    reddit.user.me()

# Otherwise, send a message to #notifications to alert to the error and close out the dbs and cursors.
except Exception as e:
    print(str(e))
    db['actions'].insert_one({'action': 'chat.postMessage', 'channel': notification_channel,
                              'message_text': 'PRAW login error on modqueue_to_db',
                              'reaction_name': "", "reaction_time": "", 'message_time': ""})

    sys.exit()

# Get reported and filtered comments
for reported_comment in reddit.subreddit('nba').mod.modqueue('comments', limit=None):
    # Extract the comment and link ids and report time
    comment_id = reported_comment.name.replace('t1_', '')
    post_id = reported_comment.link_id.replace('t3_', '')
    report_time = str(int(time.time()))

    report_reasons = []

    modqueue_message_text = ""

    # This gets the user reports and appends the appropriate text
    # reports[0] is the report reason
    for reports in reported_comment.user_reports:
        report_reasons.append(reports[0])

    # This checks if the comment was removed for reddit-wide reasons
    if reported_comment.banned_by:
        report_reasons.append("reddit-wide removal rule")

    # This checks if the comment was removed by AutoModerator and appends the appropriate text
    elif reported_comment.banned_by is not True and modqueue_message_text is not '' and \
            reported_comment.mod_reports == [] and config.automod_enabled:
        report_reasons.append(modqueue_message_text + " / AutoModerator")

    # This checks if the comment was reported by a non-AutoModerator moderator and appends the appropriate text
    # reports[0] is the report reason, reports[1] is the moderator in question
    else:
        for reports in reported_comment.mod_reports:
            report_reasons.append(reports[0] + " / " + reports[1])

    try:
        report_reasons = ", ".join(report_reasons)
    except Exception as e:
        print(str(e))
        report_reasons = ""
    report_reasons = report_reasons.replace("'", "").replace('"', '')

    # reddit URL for comment and chat message text
    url = "https://old.reddit.com/comments/" + post_id + "//" + comment_id + "?context=5"
    text = "<" + url + "|*Comment by " + reported_comment.author.name + ":* " + reported_comment.body[:200].replace("'",
                                                                                                                    "").replace(
        '"', '').replace('\n', ' ').replace('>', '').replace('<', '&lt;').replace('&', '&amp;') + ">"
    text += " (" + report_reasons + ")"

    # Check if comment is already in the db as reported
    db_check = db['modqueue'].find_one({"$and": [{'post_id': post_id}, {'comment_id': comment_id}]})
    print(db_check)
    # If the comment is not already in the db, insert the message into actions table and insert the reported comment into modqueue table
    if db_check is None:
        actions_id = db['actions'].insert_one(
            {'action': 'chat.postMessage', 'channel': modqueue_channel, 'message_text': text,
             'reaction_name': "", "reaction_time": "", 'message_time': ""})
        db['modqueue'].insert_one({'post_id': post_id, 'comment_id': comment_id, 'report_time': report_time,
                                   'slack_action_id': actions_id.inserted_id, 'reported': 'Yes', 'report_text': text,
                                   'message_time': ''})

    # # If the comment is already in the db but has not been moderated, this will delete the old message and modqueue item and redo both to bring attention.
    ## Needs work
    elif db_check is not None:
        report_time = db['modqueue'].find()[0]
        message_time = report_time['message_time']
        old_text = report_time['report_text']
        report_time = report_time['report_time']

        # If the comment has gone unmoderated for some time (per config) or a new report has come in, put it back in #mod-queue for attention.
        # if time.time() - float(report_time) >= config.mins_until_bump * 60.0 or old_text != text:
        #     report_time = str(int(time.time()))
        #     db['actions'].insert_one(
        #         {'action': 'chat.delete', 'channel': modqueue_channel, 'message_time': message_time,
        #          'reaction_name': "", "reaction_time": "", 'message_text': ""})
        #     db['modqueue'].delete_one({'$and': [{'post_id': post_id}, {'comment_id': comment_id}]})
        #     actions_id = db['actions'].insert_one({'action': 'chat.postMessage', 'channel': modqueue_channel,
        #                                            'message_text': text,
        #                                            'reaction_name': "", "reaction_time": "", 'message_time': ""})
        #     db['modqueue'].insert_one({'post_id': post_id, 'comment_id': comment_id, 'report_time': report_time,
        #                                'slack_action_id': actions_id.inserted_id, 'reported': "Yes",
        #                                'report_text': text, 'message_time': ''})

# Get reported posts
for reported_post in reddit.subreddit('nba').mod.modqueue('submissions', limit=None):
    # Extract the link id and report time
    post_id = reported_post.name.replace('t3_', '')
    report_time = str(int(time.time()))

    report_reasons = []

    for reports in reported_post.user_reports:
        report_reasons.append(reports[0])

    for reports in reported_post.mod_reports:
        report_reasons.append(reports[0] + " / " + reports[1])
    report_reasons = [x for x in report_reasons if x is not ""]
    report_reasons = ", ".join(report_reasons)

    # reddit URL for post and chat message text
    url = "https://old.reddit.com/comments/" + post_id + "//"
    text = "<" + url + "|*Submission by " + reported_post.author.name + ":* " + \
           reported_post.title[:200].replace("'","").replace('"', '').replace('\n', ' ').replace('>', '')\
               .replace('<', '&lt;').replace('&', '&amp;') + ">"
    text += " (" + report_reasons.replace("'", "").replace('"', '') + ")"

    # Check if post is already in the db as unmoderated/reported
    db_check = db['modqueue'].find_one({"$and": [{'post_id': post_id}, {'comment_id': ''}]})
    # If the post is not already in the db, insert the message into actions table and insert the reported post into modqueue table
    if db_check is None:
        actions_id = db['actions'].insert_one({'action': 'chat.postMessage', 'channel': modqueue_channel,
                                               'message_text': text, 'reaction_name': "", "reaction_time": "",
                                               'message_time': ""})
        db['modqueue'].insert_one({'post_id': post_id, 'comment_id': '', 'report_time': report_time,
                                   'slack_action_id': actions_id.inserted_id, 'reported': "Yes", 'report_text': '',
                                   'message_time': ''})

    # If the post is already in the db but has not been moderated, this will delete the old message and modqueue item
    #  and redo both to bring attention.
    elif db_check is not None:
        ## Needs work
        report_time = db['modqueue'].find()[0]
        report = report_time['post_id']
        message_time = report_time['report_time']
        report_time = report_time['report_time']
        # If the post has been reported while unmoderated or has gone unmoderated for some time (per config),
        #  put it back in #mod-queue for attention.
        if report == 'No' or time.time() - float(report_time) >= config.mins_until_bump * 60.0:
            report_time = str(int(time.time()))
            db['actions'].insert_one({'action': 'chat.delete', 'channel': modqueue_channel,
                                      'message_time': message_time, 'reaction_name': "", "reaction_time": "",
                                      'message_text': ""})
            db['modqueue'].delete_one({'$and': [{'post_id': post_id}, {'comment_id': ''}]})
            actions_id = db['actions'].insert_one({'action': 'chat.postMessage', 'channel': modqueue_channel,
                                                   'message_text': text, 'reaction_name': "", "reaction_time": "",
                                                   'message_time': ""})
            db['modqueue'].insert_one({'post_id': post_id, 'comment_id': '', 'report_time': report_time,
                                       'slack_action_id': actions_id.inserted_id, 'reported': "Yes",
                                       'message_time': ''})

# Get unmoderated posts
# for unmoderated_post in reddit.subreddit('nba').mod.unmoderated():
#     # Extract the link id and post time
#     post_id = unmoderated_post.name.replace('t3_', '')
#     report_time = str(int(time.time()))
#
#     # reddit URL for comment and chat message text
#     url = "https://old.reddit.com/comments/" + post_id + "//"
#     text = "<" + url + "|*Submission by " + unmoderated_post.author.name + ":* " + unmoderated_post.title[:100].replace(
#         "'", "").replace('"', '').replace('\n', ' ').replace('>', '').replace('<', '&lt;').replace('&', '&amp;') + ">"
#
#     # Check if the post is already in the db as unmoderated
#     db_check = db['modqueue'].find_one({"$and": [{'post_id': post_id}, {'comment_id': ''}]})
#
#     # If the post is not already in the db, insert the message into actions table and insert the unmoderated post into modqueue table
#     if db_check == None:
#         actions_id = db['actions'].insert_one({'action': 'chat.postMessage', 'channel': modqueue_channel, 'message_text': text,
#                                                'reaction_name': "", "reaction_time": "", 'message_time': ""})
#         db['modqueue'].insert_one({'post_id': post_id, 'comment_id': '', 'report_time': report_time,
#                                    'slack_action_id': actions_id.inserted_id, 'reported': "No", 'report_text':'', 'message_time': ''})
#
#     # If the comment is already in the db but has not been moderated, this will delete the old message and modqueue item and redo both to bring attention.
#     elif db_check != None:
#         report_time = db['modqueue'].find()[0]
#         message_time = report_time['report_time']
#         report_time = report_time['report_time']
#
#         # If the comment has gone unmoderated for some time (per config), put it back in #mod-queue for attention.
#         if time.time() - float(report_time) >= config.mins_until_bump * 60.0:
#             report_time = str(int(time.time()))
#             db['actions'].insert_one({'action': 'chat.delete', 'channel': modqueue_channel,
#                                       'message_time': message_time, 'reaction_name': "", "reaction_time": "", 'message_text': ""})
#             db['modqueue'].delete_one({'$and': [{'post_id': post_id}, {'comment_id': ''}]})
#             actions_id = db['actions'].insert_one({'action': 'chat.postMessage', 'channel': modqueue_channel,
#                                                'message_text': text, 'reaction_name': "", "reaction_time": "", 'message_text': ""})
#             db['modqueue'].insert_one({'post_id': post_id, 'comment_id': '', 'report_time': report_time,
#                                    'slack_action_id': actions_id.inserted_id, 'reported': "No", 'report_text':'', message_time:''})

