import praw, prawcore, sys, time, re
import config
import pymongo
client = pymongo.MongoClient(config.mongodb)
db = client.get_database()
# A list of special characters to count as word breaks.
notification_channel = config.notification_channel
special_characters_list = [' ', '`', '~', '!', '¡', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '[', '{', ']', '}', '\\', '|', ';', ':', "'", '"', ',', '<', '.', '>', '/', '?', '¿']

# Connect to the automoderator database
automod_db = pymysql.connect(host='localhost', user=config.automod_db_user, passwd=config.automod_db_pass, db=config.automod_db_name, autocommit=True, charset='utf8')
automod_cursor = automod_db.cursor()


# Get the reported phrases and add them to a list.
reported_phrases = []
automod_cursor.execute("SELECT * FROM `reported_phrases`")

for phrase in automod_cursor:
	reported_phrases.append(phrase[1])

# Get the filtered phrases and add them to a list.
filtered_phrases = []
automod_cursor.execute("SELECT * FROM `filtered_phrases`")

for phrase in automod_cursor:
	filtered_phrases.append(phrase[1])

# Connect to reddit
try:
	reddit = praw.Reddit(client_id=config.reddit_client_id,
					 client_secret=config.reddit_client_secret,
					 username=config.reddit_user,
					 password=config.reddit_pass,
					 user_agent='AutoModerator for /r/CFB v 1.0 by /u/' + config.reddit_user)
	reddit.user.me()

except:
	mod_cursor.execute("""INSERT INTO `actions` (`action`, `channel`, `message_text`) VALUES (%s, %s, %s)""", ('chat.postMessage', notification_channel, 'PRAW login error on modqueue_report_and_filter_text.py'))
	
	automod_cursor.close()
	automod_db.close()

	sys.exit()

# Get comment stream
def modqueue_message(comment_body):
	# i is a counter for how many filtered phrases were caught in a single comment
	# j is a counter for how many reported phrases were caught in a single comment
	i = 0
	j = 0
	caught_filtered_phrases = ''
	caught_reported_phrases = ''
	
	# Check if filtered phrase is found somewhere in the comment
	for phrase in filtered_phrases:
		if phrase.lower() in comment_body.lower():
			# Get the set of starting locations of the phrase in question
			locations_list = [location.start() for location in re.finditer(phrase.lower(), comment_body.lower())]
			
			for location in locations_list:
				# These conditions are just to check on either side of the phrase to see if it matches a special character (or is end of comment)
				# This prevents false positives, like 'homotopy' for filtered phrase 'homo'
				
				# In this case, the phrase starts at the beginning of the comment and is shorter than the comment.
				# The next character after the phrase is a special character.
				if location == 0 and location+len(phrase) < len(comment_body) and comment_body[location+len(phrase)] in special_characters_list:
					i += 1
					# Add the caught phrase to the string
					caught_filtered_phrases += '*'+phrase+'*, '
					# Add one to the occurrence of the phrase and update - this allows us to track the effectiveness of the rule
					automod_cursor.execute("""SELECT `count` FROM `filtered_phrases` WHERE `phrase`=%s""", phrase)
					count = int(automod_cursor.fetchall()[0][0])
					count += 1
					automod_cursor.execute("""UPDATE `filtered_phrases` SET `count`=%s WHERE `phrase`=%s""", (count, phrase))
					break
					continue
				
				# In this case, the phrase starts at the beginning of the comment and is the full comment.
				elif location == 0 and location+len(phrase) == len(comment_body):
					i += 1
					caught_filtered_phrases += '*'+phrase+'*, '
					automod_cursor.execute("""SELECT `count` FROM `filtered_phrases` WHERE `phrase`=%s""", phrase)
					count = int(automod_cursor.fetchall()[0][0])
					count += 1
					automod_cursor.execute("""UPDATE `filtered_phrases` SET `count`=%s WHERE `phrase`=%s""", (count, phrase))
					break
					continue
				
				# In this case, the phrase starts immediately after a special character (possibly a space) and ends at the end of the comment.
				elif comment_body[location-1] in special_characters_list and location+len(phrase) == len(comment_body):
					i += 1
					caught_filtered_phrases += '*'+phrase+'*, '
					automod_cursor.execute("""SELECT `count` FROM `filtered_phrases` WHERE `phrase`=%s""", phrase)
					count = int(automod_cursor.fetchall()[0][0])
					count += 1
					automod_cursor.execute("""UPDATE `filtered_phrases` SET `count`=%s WHERE `phrase`=%s""", (count, phrase))
					break
					continue
				
				# In this case, the phrase starts immediately after a special character (possibly a space) and is followed by a special character.
				elif comment_body[location-1] in special_characters_list and comment_body[location+len(phrase)] in special_characters_list:
					i += 1
					caught_filtered_phrases += '*'+phrase+'*, '
					automod_cursor.execute("""SELECT `count` FROM `filtered_phrases` WHERE `phrase`=%s""", phrase)
					count = int(automod_cursor.fetchall()[0][0])
					count += 1
					automod_cursor.execute("""UPDATE `filtered_phrases` SET `count`=%s WHERE `phrase`=%s""", (count, phrase))
					break
					continue
	
	for phrase in reported_phrases:
		if phrase.lower() in comment_body.lower():
			# Get the set of starting locations of the phrase in question
			locations_list = [location.start() for location in re.finditer(phrase.lower(), comment_body.lower())]
			
			for location in locations_list:
				# These conditions are just to check on either side of the phrase to see if it matches a special character (or is end of comment)
				# This prevents false positives, like 'homotopy' for filtered phrase 'homo'
				
				# In this case, the phrase starts at the beginning of the comment and is shorter than the comment.
				# The next character after the phrase is a special character.
				if location == 0 and location+len(phrase) < len(comment_body) and comment_body[location+len(phrase)] in special_characters_list:
					j += 1
					# Add the caught phrase to the string
					caught_reported_phrases += '*'+phrase+'*, '
					# Add one to the occurrence of the phrase and update - this allows us to track the effectiveness of the rule
					automod_cursor.execute("""SELECT `count` FROM `reported_phrases` WHERE `phrase`=%s""", phrase)
					count = int(automod_cursor.fetchall()[0][0])
					count += 1
					automod_cursor.execute("""UPDATE `reported_phrases` SET `count`=%s WHERE `phrase`=%s""", (count, phrase))
					break
					continue
				
				# In this case, the phrase starts at the beginning of the comment and is the full comment.
				elif location == 0 and location+len(phrase) == len(comment_body):
					j += 1
					caught_reported_phrases += '*'+phrase+'*, '
					automod_cursor.execute("""SELECT `count` FROM `reported_phrases` WHERE `phrase`=%s""", phrase)
					count = int(automod_cursor.fetchall()[0][0])
					count += 1
					automod_cursor.execute("""UPDATE `reported_phrases` SET `count`=%s WHERE `phrase`=%s""", (count, phrase))
					break
					continue
				
				# In this case, the phrase starts immediately after a special character (possibly a space) and ends at the end of the comment.
				elif comment_body[location-1] in special_characters_list and location+len(phrase) == len(comment_body):
					j += 1
					caught_reported_phrases += '*'+phrase+'*, '
					automod_cursor.execute("""SELECT `count` FROM `reported_phrases` WHERE `phrase`=%s""", phrase)
					count = int(automod_cursor.fetchall()[0][0])
					count += 1
					automod_cursor.execute("""UPDATE `reported_phrases` SET `count`=%s WHERE `phrase`=%s""", (count, phrase))
					break
					continue
				
				# In this case, the phrase starts immediately after a special character (possibly a space) and is followed by a special character.
				elif comment_body[location-1] in special_characters_list and comment_body[location+len(phrase)] in special_characters_list:
					j += 1
					caught_reported_phrases += '*'+phrase+'*, '
					automod_cursor.execute("""SELECT `count` FROM `reported_phrases` WHERE `phrase`=%s""", phrase)
					count = int(automod_cursor.fetchall()[0][0])
					count += 1
					automod_cursor.execute("""UPDATE `reported_phrases` SET `count`=%s WHERE `phrase`=%s""", (count, phrase))
					break
					continue
	
	# If a filtered phrase was caught, but no reported phrase was caught, do this.
	if i == 1 and j == 0:
		return '1 filtered phrase: '+caught_filtered_phrases[:-2]
	
	# If a reported phrase was caught, but no filtered phrase was caught, do this.
	elif i == 0 and j == 1:
		return '1 reported phrase: '+caught_reported_phrases[:-2]
	
	# If a reported phrase and a filtered phrase were caught, do this.
	elif i == 1 and j == 1:
		return '1 filtered phrase: '+caught_filtered_phrases[:-2]+' and 1 reported phrase: '+caught_reported_phrases[:-2]
	
	# If multiple filtered phrases were caught but no reported phrases were caught, do this.
	elif i > 1 and j == 0:
		return str(i)+' filtered phrases: '+caught_filtered_phrases[:-2]
	
	# If multiple reported phrases were caught, but no filtered phrases were caught, do this.
	elif i == 0 and j > 1:
		return str(j)+' reported phrases: '+caught_reported_phrases[:-2]
	
	elif i > 1 and j == 1:
		return str(i)+' filtered phrases: '+caught_filtered_phrases[:-2]+' and 1 reported phrase: '+caught_reported_phrases[:-2]
	
	elif i == 1 and j > 1:
		return '1 filtered phrase: '+caught_filtered_phrases[:-2]+' and '+str(j)+' reported phrases: '+caught_reported_phrases[:-2]
	
	# If multiple reported phrases and multiple filtered phrases were caught, do this.
	elif i > 1 and j > 1:
		return str(i)+' filtered phrases: '+caught_filtered_phrases[:-2]+' and '+str(j)+' reported phrases: '+caught_reported_phrases[:-2]
	
	else:
		return ''


#automod_cursor.close()
#automod_db.close()



