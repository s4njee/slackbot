import praw, prawcore, sys, time, datetime
import config
import pymongo

client = pymongo.MongoClient(config.mongodb)
db = client.get_database()

reddit = praw.Reddit(client_id=config.reddit_client_id,
                     client_secret=config.reddit_client_secret,
                     username=config.reddit_user,
                     password=config.reddit_pass,
                     user_agent='Moderation Log to Database v 1.0 by /u/' + config.reddit_user)


def suffix(n):
    if n % 10 == 1 and n % 100 != 11:
        return "st"
    elif n % 10 == 1 and n % 100 == 11:
        return "th"
    elif n % 10 == 2 and n % 100 != 12:
        return "nd"
    elif n % 10 == 2 and n % 100 == 12:
        return "th"
    elif n % 10 == 3 and n % 100 != 13:
        return "rd"
    else:
        return "th"


for item in reddit.subreddit(config.subreddit).mod.log(limit=100):
    action = item.action
    moderator = str(item.mod)
    user = str(item.target_author)
    post_link = item.target_permalink
    comment_link = item.target_fullname
    title_text = item.target_title
    comment_text = item.target_body
    time = int(item.created_utc)
    reason = item.description
    length = item.details

    if post_link != None:
        post_link = post_link.split('/')[4]

    if comment_link != None:
        comment_link = comment_link[3:]

    if title_text != None:
        title_title = title_text[:200]

    if comment_text != None:
        comment_text = comment_text[:200]

    if action == 'unbanuser' and reason == None:
        reason = ''

    if action == 'banuser' and reason == None:
        reason = 'None specified'

    if action == 'removecomment':
        try:
            db['removals'].insert_one({'action':action, 'moderator':moderator, 'user':user,
                                       'link':(post_link + '//' + comment_link), 'text':comment_text, 'time': str(time)})
            db['mod_logs'].insert_one({'action':action, 'moderator':moderator, 'user':user,
                                       'link':(post_link + '//' + comment_link), 'text':comment_text, 'time': str(time)})
        except Exception as e:
            print(str(e))
            pass

    elif action == 'removelink':
        try:
            db['removals'].insert_one({'action':action, 'moderator':moderator, 'user':user,
                                       'link':post_link,  'text': title_text, 'time': str(time)})
            db['mod_logs'].insert_one({'action':action, 'moderator':moderator, 'user':user,
                                       'link':post_link,  'text': title_text, 'time': str(time)})
        except Exception as e:
            print(str(e))
            pass

    elif action == 'banuser':
        try:
            db['bans'].insert_one({'action':action, 'moderator':moderator, 'user':user,
                                   'reason':reason, 'length': length, 'time': str(time)})

            db['mod_logs'].insert_one({'action':action, 'moderator':moderator, 'user':user,
                                   'reason':reason, 'length': length, 'time': str(time)})

            ban_count = db['bans'].find({'$and': [{'user': user},{'action': 'banuser'}]}).fetch().count()

            if length != 'permanent':
                text = user + " was just banned for the " + str(ban_count) + suffix(
                    ban_count) + " time for " + length + ":\n"
            else:
                text = user + " was just permanently banned:\n"

            text += "> " + datetime.datetime.fromtimestamp(time).strftime('%b %-d, %Y: ') + reason
            db['actions'].insert_one({'action': 'chat.postMessage', 'channel': config.ban_channel, 'message_text': text})
        except Exception as e:
            print(str(e))
            pass

    elif action == 'unbanuser':
        if reason != 'was temporary':
            try:
                db['bans'].insert_one({'action': action, 'moderator': moderator, 'user': user,
                                       'reason': reason, 'length': length, 'time': str(time)})

                db['mod_logs'].insert_one({'action': action, 'moderator': moderator, 'user': user,
                                           'reason': reason, 'length': length, 'time': str(time)})

                text = user + " was just unbanned by " + moderator

                db['actions'].insert_one({'action': 'chat.postMessage', 'channel': config.ban_channel, 'message_text': text})
            except Exception as e:
                print(str(e))
                pass

    # Rest of the mod actions - three cases depending on the action
    else:
        if post_link != None and comment_link != None and post_link != comment_link:
            db['mod_logs'].insert_one({'action': action, 'moderator': moderator, 'user': user,
                                       'link': post_link + '//' + comment_link, 'title': title_text,
                                       'text': comment_text, 'time': str(time), 'reason': reason, 'length': length})
        elif post_link != None and (comment_link == None or post_link == comment_link):
            db['mod_logs'].insert_one({'action': action, 'moderator': moderator, 'user': user,
                                       'link': post_link, 'title': title_text,
                                       'text': comment_text, 'time': str(time), 'reason': reason, 'length': length})
        else:
            db['mod_logs'].insert_one({'action': action, 'moderator': moderator, 'user': user,
                                       'link': post_link, 'title': title_text,
                                       'text': comment_text, 'time': str(time), 'reason': reason})
