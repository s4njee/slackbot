# This code reads in the items in the modqueue database.
# These are the items that have not been acted upon.
# If the item has been acted upon, add the moderator reaction and approved/removed reaction.
# If the item was deleted, add ghost and tombstone reactions.

import praw, prawcore, sys, time
import config
import pymongo

client = pymongo.MongoClient(config.mongodb)
db = client.get_database()
modqueue_channel = config.modqueue_channel
notification_channel = config.notification_channel

# Check if login worked.
# PRAW doesn't detect if a login was wrong.
# You have to *do* something with the login for PRAW to throw an error.
# Thus the reddit.user.me() line.
try:
    reddit = praw.Reddit(client_id=config.reddit_client_id,
                         client_secret=config.reddit_client_secret,
                         username=config.reddit_user,
                         password=config.reddit_pass,
                         user_agent='Moderation Reactions v 1.0 by /u/' + config.reddit_user)

    reddit.user.me()

# Otherwise, send a message to #notifications to alert to the error and close out the dbs and cursors.
except:
    db['actions'].insert_one({'action': 'chat.postMessage', 'channel': notification_channel,
                              'message_text': 'PRAW login error on modqueue_reactions'})

    sys.exit()

# Get the items in modqueue
modqueue_items = db['modqueue'].find({})

# Read in rows in modqueue
for item in modqueue_items:
    modqueue_id = item['_id']

    # If modqueue item is a comment, do this
    if item['comment_id'] != '':
        # Convert comment id to comment class
        comment = reddit.comment(item['comment_id'])

        # If comment was approved, delete row from table, add approved and moderator emojis
        if comment.author != None and comment.approved_at_utc != None and int(comment.approved_at_utc) >= int(
                item['report_time']):
            moderator = config.modmojis[str(comment.approved_by)]
            db['actions'].insert_one({'action': 'reactions.add', 'channel': modqueue_channel,
                                      'reaction_name': config.actions['approved'],
                                      'reaction_time': item['message_time'], 'message_text': ""})
            db['actions'].insert_one({'action': 'reactions.add', 'channel': modqueue_channel,
                                      'reaction_name': moderator, 'reaction_time': item['message_time'],
                                      'message_text': ""})
            db['modqueue'].delete_one({'_id': modqueue_id})

        # If comment was removed, delete row from table, add removed and moderator emojis
        elif comment.author != None and comment.banned_at_utc != None and int(comment.banned_at_utc) >= int(
                item['report_time']):
            try:
                moderator = config.modmojis[str(comment.banned_by)]
            except:
                moderator = 'AutoModerator'
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': config.actions['removed'],
                 'reaction_time': item['message_time'], 'message_text': ""})
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': moderator,
                 'reaction_time': item['message_time'], 'message_text': ""})
            db['modqueue'].delete_one({'_id': modqueue_id})

        # If comment was deleted, delete row from table, add ghost and rip emojis
        elif comment.author == None:
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': config.actions['deleted1'],
                 'reaction_time': item['message_time'], 'message_text': ""})
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': config.actions['deleted2'],
                 'reaction_time': item['message_time'], 'message_text': ""})
            db['modqueue'].delete_one({'_id': modqueue_id})


    # If modqueue item is a submission, do this
    else:
        # Convert submission id to submission class
        submission = reddit.submission(item['post_id'])

        # If submission was approved, delete row from table, add approved and moderator emojis
        if submission.author != None and submission.approved_at_utc != None and int(submission.approved_at_utc) >= int(
                item[3]):
            moderator = config.modmojis[str(submission.approved_by)]
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': config.actions['approved'],
                 'reaction_time': item['message_time'], 'message_text':""})
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': moderator,
                 'reaction_time': item['message_time'], 'message_text':""})
            db['modqueue'].delete_one({'_id': modqueue_id})

        # If submission was removed, delete row from table, add removed and moderator emojis
        elif submission.author != None and submission.banned_at_utc != None and int(submission.banned_at_utc) >= int(
                item[3]):
            try:
                moderator = config.modmojis[str(submission.banned_by)]
            except:
                moderator = 'AutoModerator'
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': config.actions['removed'],
                 'reaction_time': item['message_time'], 'message_text':""})
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': moderator,
                 'reaction_time': item['message_time'], 'message_text':""})
            db['modqueue'].delete_one({'_id': modqueue_id})

        # If submission was deleted, delete row from table, add ghost and rip emojis
        elif submission.author == None:
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': config.actions['deleted1'],
                 'reaction_time': item['message_time'], 'message_text':""})
            db['actions'].insert_one(
                {'action': 'reactions.add', 'channel': modqueue_channel, 'reaction_name': config.actions['deleted2'],
                 'reaction_time': item['message_time'], 'message_text':""})
            db['modqueue'].delete_one({'_id': modqueue_id})
