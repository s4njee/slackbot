import time, json, random, datetime, praw, prawcore, sys, base64, zlib
from random import randint
import config
from pymongo import MongoClient
import pymongo

client = pymongo.MongoClient(config.mongodb)
db = client.get_database()

notification_channel = config.notification_channel


try:
    reddit = praw.Reddit(client_id=config.reddit_client_id,
                         client_secret=config.reddit_client_secret,
                         username=config.reddit_user,
                         password=config.reddit_pass,
                         user_agent='Moderation Commands v 1.0 by /u/' + config.reddit_user)
    reddit.user.me()
except:
    db['actions'].insertOne(
        {"action": 'chat.postMessage', 'channel': notification_channel, 'message_text': 'PRAW login error on commands'})
    sys.exit()


# Get user flair
def flair(username):
    flair = db['user_flair'].find_one({'username': username})['flair']
    if (flair != None):
        return "• *Flair*: " + flair
    else:
        return "• *Flair*: "


# Check if user has read the rules
def rules(username):
    rules = db['rules'].find({'username': username})
    if len(rules) == 0:
        return "• *Rules*: I don't think " + username + " has read the rules. :x:"

    else:
        return "• *Rules*: I think " + username + " has read the rules. :white_check_mark:"


# Load usernotes
if config.notes_enabled == True:
    usernotes = json.loads(reddit.subreddit(config.subreddit).wiki['usernotes'].content_md)
    usernotes = base64.b64decode(usernotes['blob'])
    usernotes = zlib.decompress(usernotes).decode()
    usernotes = json.loads(usernotes)


def notes(username):
    notes_string = ""
    try:
        for note in usernotes[username]['ns']:
            notes_string += "\n>*" + note['n'] + "* on " + datetime.datetime.fromtimestamp(note['t']).strftime(
                '%B %d, %Y')
        return "• *Notes*: " + username + " has " + str(len(usernotes[username]['ns'])) + " notes:" + notes_string
    except:
        return "• *Notes*: " + username + " does not have any notes."


# Get user bans
def bans(username):
    bans = db['bans'].find({'user': username}).sort({'time': pymongo.DESCENDING})

    if len(bans) == 0:
        return "• *Bans*: " + username + " has not been banned. :white_check_mark:"

    else:
        bans_string = ""

        for ban in bans:
            ban_date = datetime.datetime.fromtimestamp(ban[6]).strftime('%B %d, %Y')

            if ban[1] == 'unbanuser':
                bans_string += "\n> * Unbanned by " + ban[2] + "* on " + ban_date

            elif ban[1] == 'banuser':
                ban_reason = ban[4]
                ban_length = ban[5]

                if ban_length == "":
                    ban_length = "unknown"

                if ban_reason == None:
                    bans_string += "\n> *" + ban_length + "* on " + ban_date + ": No reason specified"
                else:
                    bans_string += "\n> *" + ban_length + "* on " + ban_date + ": " + ban_reason

        bans_string = username + " has been banned " + str(len(bans)) + " times: :x:" + bans_string
        return "• *Bans*: " + bans_string


# Get user warnings
def warnings(username):
    modmail_items = db['modmail'].find({'user': username})
    warnings = []

    for item in modmail_items:
        if 'warning' in item[1] or 'Warning' in item[1] or 'this is a warning' in item[2] or 'This is a warning' in \
                item[2]:
            warnings.append(item[3])

    if len(warnings) == 0:
        return "• *Warnings*: I don't think " + username + " has any warnings. :white_check_mark:"

    else:
        if len(warnings) == 1:
            return "• *Warnings*: I think " + username + " has one warning: <https://www.reddit.com/message/messages/" + \
                   warnings[0] + "|here>. :x:"

        else:
            warnings_string = ""

            for i in range(0, len(warnings)):
                warnings_string += "\n><" + warnings[i] + "|here>"

            return "• *Warnings*: I think " + username + " has " + len(warnings) + " warnings: :x:" + warnings_string


# Get user removals
def removals(username):
    removals = db['removals'].find({'user': username}).sort({'time': pymongo.DESCENDING})
    removals_string = ""

    if len(removals) == 0:
        return "• *Removals*: I don't think " + username + " has any removals. :white_check_mark:"

    removal_count = 0

    for removal in removals:
        removal_url = "https://www.reddit.com" + removal[4]

        if removal_count < config.max_removals_shown:
            removal_count += 1
            removals_string += '\n><' + removal_url + '|' + removal[5].replace(">", "").replace("\n", " ") + '>'

    if config.has_domain == True and config.has_removed_comments_page == True:
        removals_url = config.domain + config.removed_comments_page + "?" + config.user_variable + "=" + username
        removals_string = username + " has " + str(
            len(removals)) + " removals. You can see them all <" + removals_url + "|here>. :x:" + removals_string
    else:
        removals_string = username + " has " + str(len(removals)) + " removals. Here are the " + str(
            config.max_removals_shown) + " most recent:" + removals_string
    return "• *Removals*: " + removals_string


# Ban user from Slack with CFB_Referee
def ban_user(username, length, reason, ban_message):
    print(username, length, reason, ban_message)
    if length == 0 or length == 'permanent':
        reddit.subreddit('CFB').banned.add(username, ban_reason=reason, ban_message=ban_message)
        return username + " has been *permanently banned* for _" + reason + "_."
    elif length == 1:
        reddit.subreddit('CFB').banned.add(username, duration=length, ban_reason=reason, ban_message=ban_message)
        return username + " has been *banned for 1 day* for _" + reason + "_."
    elif length > 1:
        reddit.subreddit('CFB').banned.add(username, duration=length, ban_reason=reason, ban_message=ban_message)
        return username + " has been *banned for " + str(length) + " days* for _" + reason + "_."
    else:
        print('Could not retrieve bans.')


