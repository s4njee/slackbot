# This is the config file containing db name, table name, passwords

# Login information for mod_db - necessary
mod_db_user = 'moddbuser'
mod_db_pass = 'moddbpass'
mod_db_name = 'moddbname'

mongodb = 'mongodb://userOV3:cE2LoGfcR58KPBRn@mongodb/mod_db'
# Login information for automod_db - optional if enhanced AutoMod functionality is desired
automod_db_user = 'automoddbuser'
automod_db_pass = 'automoddbpass'
automod_db_name = 'automoddbname'

mins_until_bump = 15    # Minutes until unmoderated item is bumped in modqueue
max_removals_shown = 5  # Max number of removals to show for a user

custom_commands = True  # Only applicable if secondary commands are useful
flair_enabled = True    # Only applicable if flair is important for moderation
rules_enabled = True    # Only applicable if an external rules page exists
automod_enabled = False  # Only applicable if an enhanced AutoMod is desired
notes_enabled = True    # This only works for Toolbox for now
notes_type = 'toolbox'  # 'toolbox' or 'snoonotes' - SnooNotes functionality not yet supported

# Combine these to generate the link to user removals page, e.g. https://mydomain/removals.php?user=someusername if one exists
has_domain = True
has_removed_comments_page = True
domain = 'https://mydomain/'
removed_comments_page = 'removals.php'
user_variable = 'user'

chat_client = 'slack'                          # 'slack' or 'discord'
ban_channel = 'bans'                     # Channel to report bans in. Not name of channel, but channel ID found in URL, e.g. 'channelID' in 'https://team.slack.com/messages/channelID/'
modqueue_channel = 'modqueue_nba'           # Channel for modqueue
notification_channel = 'notifications'  # Channel for notifications
client_token = 'xoxb-382337977745-382341191632-fU1UkMRwDZ70DLLOQuX2C4Li'                         # Slack or Discord token
# Slack token may be obtained from https://api.slack.com/custom-integrations/legacy-tokens

subreddit = 'nba'                      # subreddit being moderated
reddit_user = 's4njee'                   # bot user being logged into reddit
reddit_pass = 'n?&)2WHroki^3dnK,N!ywsLP'                   # bot user password
reddit_client_id = 'D8an6LSBAfeqEg'          # bot user client_id
reddit_client_secret = 'vANC7o4AXeqPswGTJxm9KwfrmqM'  # bot user client secret
# Reddit client id and secret may be obtained from https://www.reddit.com/prefs/apps/
# Go to 'create another app...'
# Name it whatever you wish
# Make sure it is a script, NOT web app and NOT installed app
# Add whatever description you wish
# Leave 'about url' empty
# Set 'redirect url' to https://www.reddit.com
# Upon refresh, you will see your app. Click edit on the app.
# Just below 'personal use script', you will find an alphanumeric mishmash - this is your client ID
# Your client secret will be listed under secret.
# NEVER, EVER share your client secret with anyone.

# Emojis for the different actions (approve, remove, user deletion) and mod-specific emojis to show which mod took which action
actions  = {  'approved': 'approved', 'removed': 'removed', 'deleted1': 'rip', 'deleted2': 'ghost'  }
modmojis = { 'AutoModerator': 'automoderatoremoji', 'LieutenantKumar': 'octopus', 'FeversMirrors': 'gorilla', 'brexbre': 'horse', 'widesheep': 'panda_face', 'iamtheraptor': 'lizard' }
